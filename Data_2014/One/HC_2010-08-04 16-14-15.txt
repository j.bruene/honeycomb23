HC_2010-08-04 16-14-15
List of players:
  /192.168.0.11:1053  0
  /192.168.0.12:1301  1
  /192.168.0.13:1085  2
  /192.168.0.14:1209  3
  /192.168.0.15:1312  4
  /192.168.0.16:1181  5
  /192.168.0.17:4008  6
  /192.168.0.10:1116  7
  /192.168.0.18:1134  8
  /192.168.0.19:1058  9


##### Spiel1: Vorübung einzeln #####

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 16:19:58,187

##### Spiel2: Vorübung gemeinsam #####  

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 16:24:18,984

##### Spiel3: ##### 

Feld   :Nuggets:NugVisible:Cleared:Players
 -5 -3 :     1 :     false:  false:
  5  5 :     1 :     false:  false:
 -5 -2 :     1 :     false:  false:
  5  0 :     1 :     false:  false:
  5  3 :     1 :     false:  false:
  4  2 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 16:27:10,015
Kontostände:
  0: 0,75 Euro
  1: 0,00 Euro
  2: 0,00 Euro
  3: 0,00 Euro
  4: 0,00 Euro
  5: 0,00 Euro
  6: 0,00 Euro
  7: 0,00 Euro
  8: 0,00 Euro
  9: 0,00 Euro

##### Spiel4: #####

Feld   :Nuggets:NugVisible:Cleared:Players
 -2 -5 :     1 :     false:  false:
  4  1 :     1 :     false:  false:
 -5 -4 :     1 :     false:  false:
  0 -3 :     1 :     false:  false:
 -3 -2 :     1 :     false:  false:
  2  5 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 16:29:55,921
Kontostände:
  0: 0,75 Euro
  1: 0,00 Euro
  2: 0,00 Euro
  3: 0,25 Euro
  4: 0,00 Euro
  5: 0,50 Euro
  6: 0,25 Euro
  7: 0,00 Euro
  8: 0,25 Euro
  9: 0,00 Euro

##### Spiel5: Experiment	##### 

Anzahl Spieler: 10
Anzahl informierter Spieler:  2
opposed=  1

StartSicht fuer  0:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     2 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  1:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     2 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  2:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  3:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  4:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  5:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  6:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  7:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  8:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  9:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 16:33:30,296
Kontostände:
  0: 4,75 Euro
  1: 1,00 Euro
  2: 4,00 Euro
  3: 4,25 Euro
  4: 4,00 Euro
  5: 4,50 Euro
  6: 4,25 Euro
  7: 4,00 Euro
  8: 4,25 Euro
  9: 1,00 Euro
