HC_2010-07-29 14-37-26
List of players:
  /192.168.0.11:1059  0
  /192.168.0.12:1272  1
  /192.168.0.13:1081  2
  /192.168.0.14:1152  3
  /192.168.0.15:1249  4
  /192.168.0.16:1154  5
  /192.168.0.17:3831  6
  /192.168.0.10:1094  7
  /192.168.0.18:1071  8
  /192.168.0.19:1069  9


##### Spiel1: Vorübung einzeln #####

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:42:23,375

##### Spiel2: Vorübung gemeinsam #####  

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:45:50,828

##### Spiel3: ##### 

Feld   :Nuggets:NugVisible:Cleared:Players
  0 -2 :     1 :     false:  false:
 -3 -5 :     1 :     false:  false:
  4  0 :     1 :     false:  false:
  3 -2 :     1 :     false:  false:
  0  2 :     1 :     false:  false:
  0  4 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:48:06,468
Kontostände:
  0: 0,25 Euro
  1: 0,25 Euro
  2: 0,25 Euro
  3: 0,25 Euro
  4: 0,25 Euro
  5: 0,25 Euro
  6: 0,25 Euro
  7: 0,25 Euro
  8: 0,00 Euro
  9: 0,00 Euro

##### Spiel4: #####

Feld   :Nuggets:NugVisible:Cleared:Players
 -4  1 :     1 :     false:  false:
  3  5 :     1 :     false:  false:
  0  1 :     1 :     false:  false:
 -2  3 :     1 :     false:  false:
  1  1 :     1 :     false:  false:
  0 -4 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:50:38,812
Kontostände:
  0: 0,25 Euro
  1: 0,50 Euro
  2: 0,25 Euro
  3: 0,50 Euro
  4: 0,75 Euro
  5: 0,25 Euro
  6: 0,25 Euro
  7: 0,25 Euro
  8: 0,25 Euro
  9: 0,00 Euro

##### Spiel5: Experiment	##### 

Anzahl Spieler: 10
Anzahl informierter Spieler:  2
opposed=  1

StartSicht fuer  0:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  1:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  2:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  3:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     2 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  4:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  5:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  6:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  7:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  8:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     2 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  9:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:52:50,937
Kontostände:
  0: 3,25 Euro
  1: 3,50 Euro
  2: 3,25 Euro
  3: 3,50 Euro
  4: 3,75 Euro
  5: 1,75 Euro
  6: 3,25 Euro
  7: 3,25 Euro
  8: 3,25 Euro
  9: 0,50 Euro
