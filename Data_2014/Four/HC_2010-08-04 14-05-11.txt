HC_2010-08-04 14-05-11
List of players:
  /192.168.0.11:1039  0
  /192.168.0.12:1222  1
  /192.168.0.13:1060  2
  /192.168.0.14:1123  3
  /192.168.0.15:1215  4
  /192.168.0.16:1120  5
  /192.168.0.17:3112  6
  /192.168.0.10:1073  7
  /192.168.0.18:1087  8
  /192.168.0.19:1045  9


##### Spiel1: Vorübung einzeln #####

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:10:33,390

##### Spiel2: Vorübung gemeinsam #####  

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:13:43,234

##### Spiel3: ##### 

Feld   :Nuggets:NugVisible:Cleared:Players
  2  1 :     1 :     false:  false:
 -2  1 :     1 :     false:  false:
 -3 -4 :     1 :     false:  false:
  1 -4 :     1 :     false:  false:
  5  0 :     1 :     false:  false:
  3  1 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:16:06,078
Kontostände:
  0: 0,00 Euro
  1: 0,25 Euro
  2: 0,50 Euro
  3: 0,50 Euro
  4: 0,25 Euro
  5: 0,00 Euro
  6: 0,50 Euro
  7: 0,50 Euro
  8: 0,00 Euro
  9: 0,25 Euro

##### Spiel4: #####

Feld   :Nuggets:NugVisible:Cleared:Players
  4  2 :     1 :     false:  false:
  3 -1 :     1 :     false:  false:
  5  5 :     1 :     false:  false:
  1  0 :     1 :     false:  false:
 -2 -2 :     1 :     false:  false:
  2 -3 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:18:09,218
Kontostände:
  0: 0,25 Euro
  1: 0,25 Euro
  2: 0,50 Euro
  3: 0,75 Euro
  4: 0,25 Euro
  5: 0,50 Euro
  6: 0,50 Euro
  7: 0,75 Euro
  8: 0,00 Euro
  9: 0,25 Euro

##### Spiel5: Experiment	##### 

Anzahl Spieler: 10
Anzahl informierter Spieler:  2
opposed=  1

StartSicht fuer  0:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     2 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  1:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  2:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  3:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  4:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  5:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     2 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  6:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  7:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  8:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  9:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 14:21:09,359
Kontostände:
  0: 10,25 Euro
  1: 5,25 Euro
  2: 5,50 Euro
  3: 5,75 Euro
  4: 5,25 Euro
  5: 10,50 Euro
  6: 5,50 Euro
  7: 5,75 Euro
  8: 5,00 Euro
  9: 5,25 Euro
