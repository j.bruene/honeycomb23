HC_2010-07-15 20-56-55
List of players:
  /192.168.0.11:1057  0
  /192.168.0.12:1285  1
  /192.168.0.13:1227  2
  /192.168.0.14:1192  3
  /192.168.0.15:1275  4
  /192.168.0.16:1204  5
  /192.168.0.17:1543  6
  /192.168.0.10:1101  7
  /192.168.0.18:1163  8
  /192.168.0.19:1113  9


##### Spiel1: Vorübung einzeln #####

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 21:01:44,937

##### Spiel2: Vorübung gemeinsam #####  

Feld   :Nuggets:NugVisible:Cleared:Players
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 21:04:25,421

##### Spiel3: ##### 

Feld   :Nuggets:NugVisible:Cleared:Players
  1  5 :     1 :     false:  false:
  2 -2 :     1 :     false:  false:
  2  0 :     1 :     false:  false:
  4  1 :     1 :     false:  false:
 -5 -2 :     1 :     false:  false:
 -2  1 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 21:06:08,250
Kontostände:
  0: 0,25 Euro
  1: 0,25 Euro
  2: 0,00 Euro
  3: 0,50 Euro
  4: 0,25 Euro
  5: 0,50 Euro
  6: 0,50 Euro
  7: 0,00 Euro
  8: 0,25 Euro
  9: 0,00 Euro

##### Spiel4: #####

Feld   :Nuggets:NugVisible:Cleared:Players
  0  5 :     1 :     false:  false:
  0  4 :     1 :     false:  false:
  2 -1 :     1 :     false:  false:
 -4 -2 :     1 :     false:  false:
  3  0 :     1 :     false:  false:
 -4 -3 :     1 :     false:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 21:08:25,625
Kontostände:
  0: 0,25 Euro
  1: 0,75 Euro
  2: 0,00 Euro
  3: 0,75 Euro
  4: 0,25 Euro
  5: 0,50 Euro
  6: 0,50 Euro
  7: 0,50 Euro
  8: 0,25 Euro
  9: 0,25 Euro

##### Spiel5: Experiment	##### 

Anzahl Spieler: 10
Anzahl informierter Spieler:  2
opposed=  1

StartSicht fuer  0:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  1:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     2 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  2:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     2 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  3:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  4:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  5:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  6:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  7:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  8:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


StartSicht fuer  9:
Feld   :Nuggets:NugVisible:Cleared:Players
  3 -3 :     1 :      true:  false:
  6  3 :     1 :      true:  false:
  3  6 :     1 :      true:  false:
 -3  3 :     1 :      true:  false:
 -6 -3 :     1 :      true:  false:
 -3 -6 :     1 :      true:  false:
  0  0 :     0 :      true:  false:  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 


Startzeit: 21:10:46,609
Kontostände:
  0: 5,25 Euro
  1: 10,75 Euro
  2: 10,00 Euro
  3: 5,75 Euro
  4: 5,25 Euro
  5: 5,50 Euro
  6: 5,50 Euro
  7: 5,50 Euro
  8: 5,25 Euro
  9: 5,25 Euro
