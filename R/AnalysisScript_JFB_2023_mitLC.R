## GGT - Data analysis script
## Marie Ritter
## July 2022

rm(list = ls())
setwd("C:/Users/johnb/OneDrive/Dokumente/honeycomb23")
#setwd("C:/Users/Wenli/OneDrive/Dokumente/honeycomb23")

library(xtable)
library(datawizard)
library(Hmisc)
library(readxl)
library(ggplot2)
library(reshape2)
library(dplyr)
library(cowplot)
library(lme4)
library(lmerTest)
library(report)
library(sjPlot)
library(psych)
library(car)
library(performance)
library(see)
library(RColorBrewer)

library(gridExtra)

#color changes
library(openintro)

ggplot2::update_geom_defaults("point", list(color = openintro::IMSCOL["blue","full"],
                                            fill = openintro::IMSCOL["blue","full"]))
ggplot2::update_geom_defaults("bar", list(fill = openintro::IMSCOL["blue","full"], 
                                          color = "#FFFFFF"))
ggplot2::update_geom_defaults("col", list(fill = openintro::IMSCOL["blue","full"], 
                                          color = "#FFFFFF"))
ggplot2::update_geom_defaults("boxplot", list(color = openintro::IMSCOL["blue","full"]))
ggplot2::update_geom_defaults("density", list(color = openintro::IMSCOL["blue","full"]))
ggplot2::update_geom_defaults("line", list(color = openintro::IMSCOL["gray", "full"]))
ggplot2::update_geom_defaults("smooth", list(color = openintro::IMSCOL["gray", "full"]))
ggplot2::update_geom_defaults("dotplot", list(color = openintro::IMSCOL["blue","full"], 
                                              fill = openintro::IMSCOL["blue","full"]))                                             fill = openintro::IMSCOL["blue","full"]))



source("R/corr_matrix_JF.R")

# read in data

move.level.coh <- read.csv("Processed_Data_FS/GT_movelevel_cohesion.csv")
move.level.indep <- read.csv("Processed_Data_FS/GT_movelevel_independence.csv")
round.level.coh <- read.csv("Processed_Data_FS/GT_roundlevel_cohesion.csv")
round.level.indep <- read.csv("Processed_Data_FS/GT_roundlevel_independence.csv")
game.level.coh <- read.csv("Processed_Data_FS/GT_gamelevel_cohesion.csv")
game.level.indep <- read.csv("Processed_Data_FS/GT_gamelevel_independence.csv")
game.level.all.qa <- read.csv2("Processed_Data_FS/GT_gamelevel_All_QA.csv")

round.level.all <- rbind(round.level.coh, round.level.indep)
move.level.all <- rbind(move.level.coh, move.level.indep)
game.level.all <- rbind(game.level.coh, game.level.indep)

game.level.all$condition.dummy <- 0
game.level.all$condition.dummy[which(game.level.all$condition == "cohesion")] <- 1
game.level.all$condition.dummy <- factor(game.level.all$condition.dummy)

round.level.all$condition.dummy <- 0
round.level.all$condition.dummy[which(round.level.all$condition == "cohesion")] <- 1
#round.level.all$condition.dummy <- factor(round.level.all$condition.dummy)

qa.data <- read.csv2("Processed_Data_FS/GT_gamelevel_All_QA.csv")
qa.data$LQScores <- as.numeric(qa.data$LQScores)
qa.data$LCScores <- as.numeric(qa.data$LCScores)
qa.data$FSScores <- as.numeric(qa.data$FSScores)
qa.data$LFScores <- as.numeric(qa.data$LFScores)
qa.data$LHScores <- as.numeric(qa.data$LHScores)
qa.data$R2_LF  <- as.numeric(qa.data$R2_LF)
qa.data$R2_LQ  <- as.numeric(qa.data$R2_LQ)
qa.data$R2_LH  <- as.numeric(qa.data$R2_LH)
qa.data$R2_LC  <- as.numeric(qa.data$R2_LC)
qa.data$MSWS  <- as.numeric(qa.data$MSWS)
qa.data$MSWS.ESW  <- as.numeric(qa.data$MSWS.ESW)
qa.data$MSWS.SWKO  <- as.numeric(qa.data$MSWS.SWKO)
qa.data$MSWS.SWKR  <- as.numeric(qa.data$MSWS.SWKR)
qa.data$MSWS.LSW  <- as.numeric(qa.data$MSWS)
qa.data$Decisiveness  <- as.numeric(qa.data$Decisiveness)
qa.data$payout <- as.numeric(qa.data$payout)
qa.data$condition.dummy <- 0
qa.data$condition.dummy[which(qa.data$condition == "cohesion")] <- 1

round.level.all <- round.level.all[round.level.all$pid != "3_5", ]
game.level.all <- game.level.all[game.level.all$pid != "3_5", ]
move.level.all <- move.level.all[move.level.all$pid != "3_5", ]

qa.data <- qa.data[qa.data$pid != "3_5", ]
game.level.all.qa <- game.level.all.qa[game.level.all.qa$pid != "3_5", ]


#
summary(qa.data)


subset0 <- qa.data[qa.data$condition == "zero", ]
subset1 <- qa.data[qa.data$condition == "one", ]
subset2 <- qa.data[qa.data$condition == "two", ]
subset3 <- qa.data[qa.data$condition == "three", ]
subset4 <- qa.data[qa.data$condition == "four", ]


t.test(subset0$LF ~ subset0$type)
t.test(subset1$LF ~ subset1$type)
t.test(subset2$LF ~ subset2$type)
t.test(subset3$LF ~ subset3$type)
t.test(subset4$LF ~ subset4$type)



# Definiere die einzigartigen Faktoren
factors <- unique(qa.data$condition)

# Erstelle für jeden Faktor einen Plot
for (factor in factors) {
  subset_data <- subset(qa.data, condition == factor)
  
  p1 <- ggplot(subset_data, aes(x = type, y = LFScores , color = type)) +
    geom_boxplot(show.legend = FALSE) +
    geom_jitter(width = 0.1) +
    labs(title = paste("Boxplot of LF Score broken down by information. Condition:", factor), x = "Type", y = "LF Score") +
    scale_color_manual(values = c(IMSCOL["blue", "full"], IMSCOL["red", "full"]))
  
  
  ggsave(paste0("C:/Users/johnb/OneDrive/Dokumente/honeycomb23/Plots_ttest/Means_LF_cond_", factor, ".jpeg"), width = 8, height = 5)
  

  
#####LQ
  
  p2 <- ggplot(subset_data, aes(x = type, y = LQScores , color = type)) +
    geom_boxplot(show.legend = FALSE) +
    geom_jitter(width = 0.1) +
    labs(title = paste("Boxplot of LQ Score broken down by information. Condition:", factor), x = "Type", y = "LQ Score") +
    scale_color_manual(values = c(IMSCOL["blue", "full"], IMSCOL["red", "full"]))
  
  
  ggsave(paste0("C:/Users/johnb/OneDrive/Dokumente/honeycomb23/Plots_ttest/Means_LQ_cond_", factor, ".jpeg"), width = 8, height = 5)
  

#####LH

  p3 <- ggplot(subset_data, aes(x = type, y = LHScores , color = type)) +
    geom_boxplot(show.legend = FALSE) +
    geom_jitter(width = 0.1) +
    labs(title = paste("Boxplot of LH Score broken down by information. Condition:", factor), x = "Type", y = "LH Score") +
    scale_color_manual(values = c(IMSCOL["blue", "full"], IMSCOL["red", "full"]))


  ggsave(paste0("C:/Users/johnb/OneDrive/Dokumente/honeycomb23/Plots_ttest/Means_LH_cond_", factor, ".jpeg"), width = 8, height = 5)


#####LC

  p4 <- ggplot(subset_data, aes(x = type, y = LCScores , color = type)) +
    geom_boxplot(show.legend = FALSE) +
    geom_jitter(width = 0.1) +
    labs(title = paste("Boxplot of LC Score broken down by information. Condition:", factor), x = "Type", y = "LC Score") +
    scale_color_manual(values = c(IMSCOL["blue", "full"], IMSCOL["red", "full"]))


  ggsave(paste0("C:/Users/johnb/OneDrive/Dokumente/honeycomb23/Plots_ttest/Means_LC_cond_", factor, ".jpeg"), width = 8, height = 5)


#####FS

  p5 <- ggplot(subset_data, aes(x = type, y = FSScores , color = type)) +
    geom_boxplot(show.legend = FALSE) +
    geom_jitter(width = 0.1) +
    labs(title = paste("Boxplot of FS Score broken down by information. Condition:", factor), x = "Type", y = "FS Score") +
    scale_color_manual(values = c(IMSCOL["blue", "full"], IMSCOL["red", "full"]))


  ggsave(paste0("C:/Users/johnb/OneDrive/Dokumente/honeycomb23/Plots_ttest/Means_FS_cond_", factor, ".jpeg"), width = 8, height = 5)
}



#############Plots mit t test

significance_stars <- function(p) {
  if (p < 0.001) return("***")
  else if (p < 0.01) return("**")
  else if (p < 0.05) return("*")
  else if (p < 0.1) return(".")
  else return("")
}

library(ggplot2)

factors <- setdiff(unique(qa.data$condition), "zero")
base_path <- "C:/Users/johnb/OneDrive/Dokumente/honeycomb23/Plots_ttest/"

score_vars <- list(
  LFScores = "LF Score",
  LQScores = "LQ Score",
  LHScores = "LH Score",
  LCScores = "LC Score",
  FSScores = "FS Score"
)

for (factor in factors) {
  # Erstelle Unterordner für die aktuelle Bedingung
  condition_path <- paste0(base_path, "Condition_", factor, "/")
  if(!dir.exists(condition_path)) {
    dir.create(condition_path, showWarnings = FALSE)
  }
  
  subset_data <- subset(qa.data, condition == factor)
  
  for(score_var in names(score_vars)) {
    
    # Durchführung des t-Tests
    group1 <- subset_data[subset_data$type == "informed", ][[score_var]]
    group2 <- subset_data[subset_data$type == "uninformed", ][[score_var]]
    test_results <- t.test(group1, group2)
    p_value_stars <- significance_stars(test_results$p.value)
    
    # Erstellen des Plots
    p <- ggplot(subset_data, aes(x = type, y = subset_data[[score_var]], color = type)) +
      geom_boxplot(show.legend = FALSE) +
      geom_jitter(width = 0.1) +
      labs(
        title = paste("Boxplot of", score_vars[[score_var]], "broken down by information. Condition:", factor),
        subtitle = paste("t-test p-value:", round(test_results$p.value, 4), p_value_stars),
        x = "Type",
        y = score_vars[[score_var]]
      ) +
      scale_color_manual(values = c(IMSCOL["blue", "full"], IMSCOL["red", "full"]))
    
    # Speichern des Plots im jeweiligen Unterordner
    file_name <- paste0(condition_path, "Means_", substr(score_var, 1, 2), "_cond_", factor, ".jpeg")
    ggsave(file_name, width = 8, height = 5)
  }
}









######################

ggplot(data = subset0 , mapping = aes(x = FSScores, y = payout, color = type)) +
  geom_point(aes(size=0.000001))
ggplot(data = subset1 , mapping = aes(x = FSScores, y = payout, color = type)) +
  geom_point(aes(size=0.000001))
ggplot(data = subset2 , mapping = aes(x = FSScores, y = payout, color = type)) +
  geom_point(aes(size=0.000001))
ggplot(data = subset3 , mapping = aes(x = FSScores, y = payout, color = type)) +
  geom_point(aes(size=0.000001))
ggplot(data = subset4 , mapping = aes(x = FSScores, y = payout, color = type)) +
  geom_point(aes(size=0.000001))


str(subset0)


grid.arrange(p0, p1, p2, p3, p4, ncol = 5)

##################################

p0 <- ggplot(data = subset0 , mapping = aes(x = type, y = LCScores



)) +
  geom_point()
p1 <- ggplot(data = subset1 , mapping = aes(x = type, y = LCScores



)) +
  geom_point()
p2 <- ggplot(data = subset2 , mapping = aes(x = type, y = LCScores



)) +
  geom_point()
p3 <- ggplot(data = subset3 , mapping = aes(x = type, y = LCScores



)) +
  geom_point()
p4 <- ggplot(data = subset4 , mapping = aes(x = type, y = LCScores



)) +
  geom_point()




grid.arrange(p0, p1, p2, p3, p4, ncol = 5)


summary(subset0)







########
## Sample
########
qa.data$Age

mean(qa.data$Age)
sd(qa.data$Age)

report(qa.data$Gender)
report(qa.data$Job)

report(qa.data$Language)
# -> One person did not report required Language competency. Exclude!

mean(qa.data$Age)
sd(qa.data$Age)

report(qa.data$Gender)
report(qa.data$Job)

report(qa.data$Language)

########
## Descriptives
########

describe(round.level.all)
describe(game.level.all)

## The values for asymmetry and kurtosis between -2 and +2 are considered acceptable
# in order to prove normal univariate distribution (George & Mallery, 2010).
# Hair et al. (2010) and Bryne (2010) argued that data is considered to be normal
# if skewness is between ‐2 to +2 and kurtosis is between ‐7 to +7.

cor.scores <- cor(game.level.all[, c(5:15, 21)])
game.level.all[, c(5:15, 21)]

corrplot::corrplot(cor.scores)
heatmap(cor.scores)



########
## HYPOTHESIS TESTS
########



######
## Hypotheses Proof of Concept
######

qa.data[qa.data == -98] <- NA
game.level.all.qa.2 <- game.level.all.qa[, 25:58]
game.level.all.qa.2[game.level.all.qa.2 == -98] <- NA
game.level.all.qa <- cbind(game.level.all.qa[, 1:24], game.level.all.qa.2)



# H7: There will be a positive correlation between self-confidence and leadership as measured by L-F-profiles. 

str(qa.data)

qa.data$MSWS<- as.numeric(qa.data$MSWS)
qa.data$MSWS.ESW<- as.numeric(qa.data$MSWS.ESW)
qa.data$MSWS.SWKO<- as.numeric(qa.data$MSWS.SWKO)
qa.data$MSWS.SWKR<- as.numeric(qa.data$MSWS.SWKR)
qa.data$MSWS.LSW<- as.numeric(qa.data$MSWS.LSW)
qa.data$LQScores <- as.numeric(qa.data$LQScores)
###
cor.test(qa.data$LFScores, qa.data$MSWS)
cor.test(qa.data$LQScores, qa.data$MSWS)
cor.test(qa.data$LHScores, qa.data$MSWS)
cor.test(qa.data$FSScores, qa.data$MSWS)


plot(qa.data$LFScores, qa.data$MSWS)
plot(qa.data$LQScores, qa.data$MSWS)
plot(qa.data$LHScores, qa.data$MSWS)
plot(qa.data$FSScores, qa.data$MSWS)


cor.test(qa.data$LFScores, qa.data$MSWS.ESW)
cor.test(qa.data$LQScores, qa.data$MSWS.ESW)
cor.test(qa.data$LHScores, qa.data$MSWS.ESW)
plot(qa.data$LFScores, qa.data$MSWS.ESW)
plot(qa.data$LQScores, qa.data$MSWS.ESW)
plot(qa.data$LHScores, qa.data$MSWS.ESW)

cor.test(qa.data$LFScores, qa.data$MSWS.SWKO)
cor.test(qa.data$LQScores, qa.data$MSWS.SWKO)
cor.test(qa.data$LHScores, qa.data$MSWS.SWKO)
cor.test(qa.data$FSScores, qa.data$MSWS.SWKO)
plot(qa.data$LFScores, qa.data$MSWS.SWKO)
plot(qa.data$LQScores, qa.data$MSWS.SWKO)
plot(qa.data$LHScores, qa.data$MSWS.SWKO)

cor.test(qa.data$LFScores, qa.data$MSWS.SWKR)
cor.test(qa.data$LQScores, qa.data$MSWS.SWKR)
cor.test(qa.data$LHScores, qa.data$MSWS.SWKR)
plot(qa.data$LFScores, qa.data$MSWS.SWKR)
plot(qa.data$LQScores, qa.data$MSWS.SWKR)
plot(qa.data$LHScores, qa.data$MSWS.SWKR)

cor.test(qa.data$LFScores, qa.data$MSWS.LSW)
cor.test(qa.data$LQScores, qa.data$MSWS.LSW)
cor.test(qa.data$LHScores, qa.data$MSWS.LSW)
plot(qa.data$LFScores, qa.data$MSWS.LSW)
plot(qa.data$LQScores, qa.data$MSWS.LSW)
plot(qa.data$LHScores, qa.data$MSWS.LSW)


# H8: There will be a positive correlation between decisiveness and leadership as measured by L-F-profiles.
###
qa.data$Decisiveness <- as.numeric(qa.data$Decisiveness)

cor.test(qa.data$LFScores, qa.data$Decisiveness)
cor.test(qa.data$LQScores, qa.data$Decisiveness)
cor.test(qa.data$LHScores, qa.data$Decisiveness)
cor.test(qa.data$FSScores, qa.data$Decisiveness)
plot(qa.data$LFScores, qa.data$Decisiveness)
plot(qa.data$LQScores, qa.data$Decisiveness)
plot(qa.data$LHScores, qa.data$Decisiveness)
plot(qa.data$FSScores, qa.data$Decisiveness)

# H9: There will be a positive correlation between profit/achievement maximization and leadership as measured by L-F-profiles.
###
qa.data$LMI

cor.test(qa.data$LFScores, qa.data$LMI)
cor.test(qa.data$LQScores, qa.data$LMI)
cor.test(qa.data$LHScores, qa.data$LMI)
plot(qa.data$LFScores, qa.data$LMI)
plot(qa.data$LQScores, qa.data$LMI)
plot(qa.data$LHScores, qa.data$LMI)


# H10: There will be a positive correlation between risk propensity and leadership as measured by L-F-profiles.
###

cor.test(qa.data$LFScores, qa.data$RiskPropensity)
cor.test(qa.data$LQScores, qa.data$RiskPropensity)
cor.test(qa.data$LHScores, qa.data$RiskPropensity)
plot(qa.data$LFScores, qa.data$RiskPropensity)
plot(qa.data$LQScores, qa.data$RiskPropensity)
plot(qa.data$LHScores, qa.data$RiskPropensity)


# H11: There will be a positive correlation between perceived leadership/followership and leadership as measured by L-F-profiles.
###

# Variable Names - Lead/Follow are reversed - CHANGE BEFORE UPLOAD!

cor.test(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Lead)
cor.test(qa.data$LQScores, qa.data$Leadership.ObserveSelf.Lead)
cor.test(qa.data$LHScores, qa.data$Leadership.ObserveSelf.Lead)
cor.test(qa.data$FSScores, qa.data$Leadership.ObserveSelf.Lead)
# Did you lead others?
plot(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Follow)

plot(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Role)


model <- lm(Leadership.ObserveSelf.Role ~ LQScores + FSScores, data = qa.data)
summary_table = summary(model)


# Erstellen Sie eine Latex-Tabelle mit xtable
library(xtable)
xtable(summary_table, caption = "Lineare Regressionsergebnisse") 

install.packages("stargazer")
library(stargazer)


model1 <- lm(Leadership.ObserveSelf.Role ~ LFScores , data = qa.data)
model2 <- lm(Leadership.ObserveSelf.Role ~ LFScores + LQScores, data = qa.data)
model3 <- lm(Leadership.ObserveSelf.Role ~ LFScores + LQScores + LHScores, data = qa.data)
model4 <- lm(Leadership.ObserveSelf.Role ~ FSScores, data = qa.data)
model5 <- lm(Leadership.ObserveSelf.Role ~ LQScores + FSScores, data = qa.data)

summary(model5)
model1 <- lm(Leadership.ObserveSelf.Lead ~ LFScores , data = qa.data)
model2 <- lm(Leadership.ObserveSelf.Lead ~ LFScores + LQScores, data = qa.data)
model3 <- lm(Leadership.ObserveSelf.Lead ~ LFScores + LQScores + LHScores, data = qa.data)
model4 <- lm(Leadership.ObserveSelf.Lead ~ FSScores, data = qa.data)


logit_model <- glm(Leadership.ObserveSelf.Lead ~ LQScores + FSScores, data = qa.data, family = binomial())


# Berechnen Sie die vorhergesagten Wahrscheinlichkeiten für Ihre abhängige Variable
qa.data$newdata <- predict(logit_model, newdata = qa.data, type = "response")

# Plotten Sie das Ergebnis
ggplot(qa.data, aes(x = LQScores, y = qa.data$newdata)) +
  geom_line() +
  xlab("Unabhängige Variable") +
  ylab("Vorhergesagte Wahrscheinlichkeiten")

# Zeigen Sie eine Zusammenfassung des Modells an
summary(logit_model)

# Erstellen Sie eine Latex-Tabelle mit stargazer
stargazer(model1, model2, model3, model4, title = "Lineare Regressionsergebnisse", type = "latex",
          dep.var.caption = "Leadership.ObserveSelf.Lead", dep.var.labels.include = T,
          covariate.labels = c("LFScores", "LQScores", "LHScores", "FSScores"),
          font.size = "footnotesize", header = FALSE, digits=3)



ggplot(qa.data, aes(x = LFScores, y = Leadership.ObserveSelf.Role)) +
  geom_point() + 
  geom_smooth(method = "lm", se = TRUE) +
  labs(
    x = "LF Scores",
    y = "Self observed Role (1 = Follower, 7 = Leader)"
  )

ggplot(qa.data, aes(x = LQScores, y = Leadership.ObserveSelf.Role)) +
  geom_point() + 
  geom_smooth(method = "lm", se = TRUE) +
  labs(
    x = "LQ Scores",
    y = "Self observed Role (1 = Follower, 7 = Leader)"
  )

ggplot(qa.data, aes(x = LHScores, y = Leadership.ObserveSelf.Role)) +
  geom_point() + 
  geom_smooth(method = "lm", se = TRUE) +
  labs(
    x = "LH Scores",
    y = "Self observed Role (1 = Follower, 7 = Leader)"
  )

ggplot(qa.data, aes(x = FSScores, y = Leadership.ObserveSelf.Role)) +
  geom_point() + 
  geom_smooth(method = "lm", se = TRUE) +
  labs(
    x = "FS Scores",
    y = "Self observed Role (1 = Follower, 7 = Leader)"
  )











ggplot(qa.data, aes(x = LFScores, y = Leadership.ObserveSelf.Lead)) +
  geom_point() + 
  stat_smooth(method="glm", method.args=list(family="binomial"), se=FALSE) +
  labs(
    x = "LF Scores",
    y = "Self observed Followership"
  )
ggplot(qa.data, aes(x = LQScores, y = Leadership.ObserveSelf.Lead)) +
  geom_point() + 
  stat_smooth(method="glm", method.args=list(family="binomial"), se=FALSE) +
  labs(
    x = "LQ Scores",
    y = "Self observed Followership"
  )
ggplot(qa.data, aes(x = LHScores, y = Leadership.ObserveSelf.Lead)) +
  geom_point() + 
  stat_smooth(method="glm", method.args=list(family="binomial"), se=FALSE) +
  labs(
    x = "LH Scores",
    y = "Self observed Followership"
  )
ggplot(qa.data, aes(x = FSScores, y = Leadership.ObserveSelf.Lead)) +
  geom_point() + 
  stat_smooth(method="glm", method.args=list(family="binomial"), se=FALSE) +
  labs(
    x = "FS Scores",
    y = "Self observed Followership"
  )

# r = -0.199

cor.test(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Follow)
cor.test(qa.data$LQScores, qa.data$Leadership.ObserveSelf.Follow)
cor.test(qa.data$LHScores, qa.data$Leadership.ObserveSelf.Follow)# Did you follow others?
plot(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Follow)
# r = 0.268, p = 0.027

cor.test(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Role)
cor.test(qa.data$LQScores, qa.data$Leadership.ObserveSelf.Role)
cor.test(qa.data$LHScores, qa.data$Leadership.ObserveSelf.Role)
# I took a more following (0) / leading (1) role
plot(qa.data$LFScores, qa.data$Leadership.ObserveSelf.Role)
# r = 0.324, p = .001


qa.data$LQScores <- qa.data$LQScores *-1
qa.data[, c(9:12, 42:44, 50:57)]
qa.data[, c(9:13, 43:45, 51:58)]

qa.data$Goals <- as.numeric(qa.data$Goals)
qa.data$Decisiveness <- as.numeric(qa.data$Decisiveness)

str(qa.data[, c(9:13, 43:45, 51:58)])
lead.cor <- round(cor(qa.data[, c(9:13, 43:45, 51:58)], use = "pairwise.complete.obs"), 3)
lead.cor[upper.tri(lead.cor)] <- ""
lead.cor <- as.data.frame(lead.cor)

# install and load Hmisc package



# print the correlation matrix with significance levels
print(corr_matrix$R)
print(corr_matrix$P)

library(xtable)


# convert the matrix to a data frame
corr_df <- as.data.frame(corr_matrix)

# create the LaTeX table using xtable
latex_table <- xtable(lead.cor)

# print the table
print(latex_table, include.rownames = TRUE, booktabs = TRUE)

??cor




save_correlation_matrix(qa.data[, c(9:13, 43:45, 51:58)], "lead_cor_matrix_LC.csv", use = "lower", replace_diagonal = TRUE)
correlation_matrix(qa.data[, c(9:13, 43:45, 51:58)], "NEU_cor_matrix_LC.csv", use = "upper", replace_diagonal = TRUE)

corrplot::corrplot(cor(qa.data[, c(9:13, 43:45, 51:58)], use = "pairwise.complete.obs"), )

# H12: Entitativity (as measured by the mean of all group members’ reports) will be higher in the cohesion condition, compared to the independence condition.
###


t.test(qa.data$Entitativity.Diagram ~ qa.data$condition)
report(t.test(qa.data$Entitativity.Diagram ~ qa.data$condition))

# Exploratory
t.test(qa.data$Interaction.Start ~ qa.data$condition)
t.test(qa.data$Interaction.During ~ qa.data$condition)
t.test(qa.data$Interaction.None ~ qa.data$condition)
t.test(qa.data$Interaction.1_10 ~ qa.data$condition)
t.test(qa.data$Interaction.11_20 ~ qa.data$condition)
t.test(qa.data$Interaction.21_30 ~ qa.data$condition)
t.test(qa.data$Entitivity ~ qa.data$condition)
t.test(qa.data$Similarity ~ qa.data$condition)
t.test(qa.data$Interactivity ~ qa.data$condition)
t.test(qa.data$Goals ~ qa.data$condition)


# H13: Cooperation and interaction (as measured by mean of all group members’ reports) in the rounds 1 through 10 will be lower than in rounds 11 through 20 and 21 through 30; cooperation and interaction in the rounds 11 through 21 will be lower than the rounds 21 through 30. 
###

report(t.test(qa.data$Interaction.1_10, qa.data$Interaction.11_20, paired = TRUE))
mean(qa.data$Interaction.1_10)
mean(qa.data$Interaction.11_20)
mean(qa.data$Interaction.21_30)
report(t.test(qa.data$Interaction.1_10, qa.data$Interaction.21_30, paired = TRUE))
report(t.test(qa.data$Interaction.11_20, qa.data$Interaction.21_30, paired = TRUE))

# Group Means
qa.data.group.interaction <- data.frame(group = rep(unique(qa.data$group), each = 3),
                                        time = factor(rep(c("1_10", "11_20", "21_30"), times = length(unique(qa.data$group)))),
                                        value = rep(NA, times = 3*length(unique(qa.data$group))))


for (i in unique(qa.data$group)) {
  
  lines <- which(qa.data$group == i)
  lines.new <- which(qa.data.group.interaction == i)
  
  qa.data.group.interaction[lines.new[1], 3] <- mean(qa.data$Interaction.1_10[lines])
  qa.data.group.interaction[lines.new[2], 3] <- mean(qa.data$Interaction.11_20[lines])
  qa.data.group.interaction[lines.new[3], 3] <- mean(qa.data$Interaction.21_30[lines])
  
}


tiff("Figure6.tiff", unit = "cm", width = 15, height = 15, res = 350)
ggplot(data = qa.data.group.interaction, aes(x = time, y = value)) +
  geom_violin() +
  geom_boxplot(width = 0.2) +
  geom_dotplot(binaxis = "y", stackdir = "center", binwidth = 0.2) +
  labs(x = "Rounds", y = "Interaction Rating") +
  scale_x_discrete(labels = c("1 to 10", "11 to 20", "21 to 30")) +
  scale_y_continuous(limits = c(1,7), breaks = c(1, 3, 5, 7)) +
  theme_minimal() +
  theme(text = element_text(size = 12))
dev.off()


####
## EXPLORATORY
####

#### QA Data
summary(qa.data)

hist(qa.data$MSWS)
hist(qa.data$MSWS.ESW)
hist(qa.data$MSWS.SWKO)
hist(qa.data$MSWS.SWKR)
hist(qa.data$MSWS.LSW)
hist(qa.data$LMI)
hist(qa.data$RiskPropensity)
hist(qa.data$Decisiveness)
hist(qa.data$LFScores)
hist(qa.data$LQScores)
hist(qa.data$LHScores)
hist(qa.data$FSScores)


t.test(qa.data$Strategy.IntRat[which(qa.data$Strategy.IntRat != -98)] ~ qa.data$condition[which(qa.data$Strategy.IntRat != -98)])
t.test(qa.data$Risk ~ qa.data$condition)
t.test(qa.data$ChoiceField.Best.Confidence ~ qa.data$condition)

ggplot(data = game.level.all.qa, aes(x = condition, y = ChoiceField.Best.Confidence)) +
  geom_boxplot()

sum(qa.data$ColorComp == qa.data$ChoiceField.Best)/nrow(qa.data)
# 70.52 % established the correct reward field
mean(qa.data$ChoiceField.Best.Confidence[which(qa.data$ColorComp == qa.data$ChoiceField.Best)])
# with a confidence of 57.10% on average
sd(qa.data$ChoiceField.Best.Confidence[which(qa.data$ColorComp == qa.data$ChoiceField.Best)])

table(qa.data$condition[which(qa.data$ColorComp == qa.data$ChoiceField.Best)])
# about the same

sum(qa.data$ColorSec == qa.data$ChoiceField.Best)/nrow(qa.data)
# 9.47%

sum(qa.data$ColorRisk == qa.data$ChoiceField.Best)/nrow(qa.data)
# 12.63%

sum(qa.data$ColorIncomp == qa.data$ChoiceField.Best)/nrow(qa.data)
# 7.36%

mean(qa.data$ChoiceField.Best.Confidence[which(qa.data$ColorComp != qa.data$ChoiceField.Best)])
# 27.5%
sd(qa.data$ChoiceField.Best.Confidence[which(qa.data$ColorComp != qa.data$ChoiceField.Best)])


qa.data$correct.field <- 1
qa.data$correct.field[qa.data$ColorComp != qa.data$ChoiceField.Best] <- 0

leveneTest(qa.data$ChoiceField.Best.Confidence ~ factor(qa.data$correct.field))
report(t.test(qa.data$ChoiceField.Best.Confidence ~ qa.data$correct.field, var.equal = FALSE))
# significant difference
hist(qa.data$ChoiceField.Best.Confidence)
report(wilcox.test(qa.data$ChoiceField.Best.Confidence ~ qa.data$correct.field, var.equal = FALSE))


t.test(qa.data$FieldDistribution ~ qa.data$correct.field)
#less cohesion in those groups that found the correct field
# but not when looking at the move level so probably not due to fact they stuck together?

t.test(qa.data$FieldDistribution.MoveLevel ~ qa.data$correct.field)
t.test(qa.data$transitivity ~ qa.data$correct.field)
t.test(qa.data$LFScores ~ qa.data$correct.field)
t.test(qa.data$Leadership.ObserveSelf.Lead ~ qa.data$correct.field)
t.test(qa.data$Leadership.ObserveSelf.Follow ~ qa.data$correct.field)
t.test(qa.data$Leadership.ObserveSelf.Role ~ qa.data$correct.field)

t.test(qa.data$HalfChangeRound ~ qa.data$correct.field)
# again: task too easy

t.test(qa.data$Strategy.IntRat ~ qa.data$correct.field)
# Rational strategy changes were better

chisq.test(qa.data$Strategy.YN, qa.data$correct.field)
table(qa.data$Strategy.YN, qa.data$correct.field)
# those who reported a clear strategy were better

cor.test(qa.data$Entitativity.Diagram, qa.data$transitivity)
cor.test(qa.data$Entitivity, qa.data$transitivity)
cor.test(qa.data$Similarity, qa.data$transitivity)
cor.test(qa.data$Goals, qa.data$transitivity)
cor.test(qa.data$Interactivity, qa.data$transitivity)
report(cor.test(qa.data$FieldDistribution, qa.data$DecisionScore))
plot(qa.data$FieldDistribution, qa.data$DecisionScore)
cor.test(qa.data$FieldDistribution.MoveLevel, qa.data$DecisionScore)
plot(qa.data$FieldDistribution, qa.data$DecisionScore)
cor.test(qa.data$transitivity, qa.data$DecisionScore)
plot(qa.data$transitivity, qa.data$DecisionScore)


cor.test(qa.data$Entitativity.Diagram, qa.data$FieldDistribution)
cor.test(qa.data$Entitativity.Diagram, qa.data$FieldDistribution.MoveLevel)
cor.test(qa.data$Entitivity, qa.data$FieldDistribution)
cor.test(qa.data$Entitivity, qa.data$FieldDistribution.MoveLevel)
cor.test(qa.data$Similarity, qa.data$FieldDistribution)
cor.test(qa.data$Similarity, qa.data$FieldDistribution.MoveLevel)
cor.test(qa.data$Goals, qa.data$FieldDistribution)
cor.test(qa.data$Goals, qa.data$FieldDistribution.MoveLevel)
cor.test(qa.data$Interactivity, qa.data$FieldDistribution)
cor.test(qa.data$Interactivity, qa.data$FieldDistribution.MoveLevel)

groupy.cor <- round(cor(qa.data[, c(4, 8, 10, 11, 31:33, 42:46)]), 3)
groupy.cor[lower.tri(groupy.cor)] <- ""
groupy.cor <- as.data.frame(groupy.cor)

save_correlation_matrix(qa.data[, c(59, 8, 10, 11, 31:33, 42:46)], "groupy_cor_matrix.csv", use = "lower", replace_diagonal = TRUE)
correlation_matrix(qa.data[, c(8, 10, 11, 31:33, 42:46)], "cor_matrix.csv", use = "upper", replace_diagonal = TRUE)

corrplot::corrplot(cor(qa.data[, c(8, 10, 11, 31:33, 42:46)]))


# identify leader with highest & second highest LF score
leaders <- character(length = length(unique(qa.data$group)))

for (i in unique(qa.data$group)) {
  
  group.dat <- qa.data[which(qa.data$group == i), ]
  
  leader <- group.dat$pid[which(group.dat$LFScores == max(group.dat$LFScores))]
  
  leaders[i] <- leader
  
}

leaders.2 <- character(length = length(unique(qa.data$group)))

for (i in unique(qa.data$group)) {
  
  group.dat <- qa.data[which(qa.data$group == i & !(qa.data$pid %in% leaders)), ]
  
  leader <- group.dat$pid[which(group.dat$LFScores == max(group.dat$LFScores))]
  
  leaders.2[i] <- leader
  
}

followers <- character(length = length(unique(qa.data$group)))

for (i in unique(qa.data$group)) {
  
  group.dat <- qa.data[which(qa.data$group == i), ]
  
  follower <- group.dat$pid[which(group.dat$LFScores == min(group.dat$LFScores))]
  
  followers[i] <- leader
  
}

followers.2 <- character(length = length(unique(qa.data$group)))

for (i in unique(qa.data$group)) {
  
  group.dat <- qa.data[which(qa.data$group == i & !(qa.data$pid %in% leaders)), ]
  
  follower <- group.dat$pid[which(group.dat$LFScores == min(group.dat$LFScores))]
  
  followers.2[i] <- follower
  
}


qa.data$Leader <- 0
qa.data$Leader[which(qa.data$pid %in% leaders)] <- 1

qa.data$Leader.2 <- qa.data$Leader
qa.data$Leader.2[which(qa.data$pid %in% leaders.2)] <- 1

qa.data$Follower <- 0
qa.data$Follower[which(qa.data$pid %in% followers)] <- 1

qa.data$Follower.2 <- qa.data$Follower
qa.data$Follower.2[which(qa.data$pid %in% followers.2)] <- 1


qa.data$LeadFollowCompare <- NA
qa.data$LeadFollowCompare[which(qa.data$Leader.2 == 1)] <- 1
qa.data$LeadFollowCompare[which(qa.data$Follower.2 == 1)] <- 2



t.test(RiskPropensity ~ Leader, data = qa.data)
t.test(RiskPropensity ~ Leader.2, data = qa.data)
t.test(RiskPropensity ~ LeadFollowCompare, data = qa.data[!is.na(qa.data$LeadFollowCompare), ])

t.test(Decisiveness ~ Leader, data = qa.data)
t.test(Decisiveness ~ Leader.2, data = qa.data)
t.test(Decisiveness ~ LeadFollowCompare, data = qa.data[!is.na(qa.data$LeadFollowCompare), ])

t.test(LMI ~ Leader, data = qa.data)
t.test(LMI ~ Leader.2, data = qa.data)
t.test(LMI ~ LeadFollowCompare, data = qa.data[!is.na(qa.data$LeadFollowCompare), ])

t.test(MSWS ~ Leader, data = qa.data)
t.test(MSWS ~ Leader.2, data = qa.data)
t.test(MSWS ~ LeadFollowCompare, data = qa.data[!is.na(qa.data$LeadFollowCompare), ])

t.test(MSWS.ESW ~ Leader, data = qa.data)
t.test(MSWS.ESW ~ Leader.2, data = qa.data)

t.test(MSWS.SWKO ~ Leader, data = qa.data)
t.test(MSWS.SWKO ~ Leader.2, data = qa.data)

t.test(MSWS.SWKR ~ Leader, data = qa.data)
t.test(MSWS.SWKR ~ Leader.2, data = qa.data)

t.test(Leadership.ObserveSelf.Role ~ Leader, data = qa.data)
t.test(Leadership.ObserveSelf.Role ~ Leader.2, data = qa.data)
t.test(Leadership.ObserveSelf.Role ~ LeadFollowCompare, data = qa.data[!is.na(qa.data$LeadFollowCompare), ])


t.test(Leadership.ObserveSelf.Lead ~ Leader, data = qa.data)
t.test(Leadership.ObserveSelf.Lead ~ Leader.2, data = qa.data)
t.test(Leadership.ObserveSelf.Lead ~ LeadFollowCompare, data = qa.data[!is.na(qa.data$LeadFollowCompare), ])
hist(qa.data$Leadership.ObserveSelf.Lead)
hist(qa.data$Leadership.ObserveSelf.Role)

report(cor.test(qa.data$DecisionScore, qa.data$LFScores))

partial.r(qa.data[, c(9, 10, 12)]) # no difference








































###
# Do we actually find establishing leadership? --> DISSERTATION

# Calculate a cumulative LF score

round.level.all$LFScoresCum <- NA

for (i in unique(round.level.all$pid)) {
  
  round.level.all$LFScoresCum[round.level.all$pid == i] <- cumsum(round.level.all$LFScores[round.level.all$pid == i])
  
}

tiff("LFcumul.tiff", unit = "cm", res= 350,  width = 15, height = 15)
ggplot(data = round.level.all, aes(x = round, y = LFScoresCum)) +
  geom_line(aes(color = pid)) +
  facet_wrap(vars(group)) +
  theme_minimal() +
  labs(y = "Cumulative L-F-Scores", x = "Round") +
  theme(legend.position = "None", text = element_text(size = 12))
dev.off()

qa.data$LeadEst <- 1
qa.data$LeadEst[qa.data$group %in% c(3, 4, 5)] <- 0

# Calculate a cumulative LQ score

round.level.all$LQScoresCum <- NA

for (i in unique(round.level.all$pid)) {
  
  round.level.all$LQScoresCum[round.level.all$pid == i] <- cumsum(round.level.all$LQScores[round.level.all$pid == i])
  
}

tiff("LQcumul.tiff", unit = "cm", res= 350,  width = 15, height = 15)
ggplot(data = round.level.all, aes(x = round, y = LQScoresCum)) +
  geom_line(aes(color = pid)) +
  facet_wrap(vars(group)) +
  theme_minimal() +
  labs(y = "Cumulative L-Q-Scores", x = "Round") +
  theme(legend.position = "None", text = element_text(size = 12))
dev.off()

qa.data$LeadEst <- 1
qa.data$LeadEst[qa.data$group %in% c(3, 4, 5)] <- 0

# Calculate a cumulative LH score

round.level.all$LHScoresCum <- NA

for (i in unique(round.level.all$pid)) {
  
  round.level.all$LHScoresCum[round.level.all$pid == i] <- cumsum(round.level.all$LHScores[round.level.all$pid == i])
  
}

tiff("LHcumul.tiff", unit = "cm", res= 350,  width = 15, height = 15)
ggplot(data = round.level.all, aes(x = round, y = LHScoresCum)) +
  geom_line(aes(color = pid)) +
  facet_wrap(vars(group)) +
  theme_minimal() +
  labs(y = "Cumulative L-H-Scores", x = "Round") +
  theme(legend.position = "None", text = element_text(size = 12))
dev.off()

qa.data$LeadEst <- 1
qa.data$LeadEst[qa.data$group %in% c(3, 4, 5)] <- 0

#t.test(qa.data$Account ~ qa.data$LeadEst)
report(t.test(qa.data$DecisionScore ~ qa.data$LeadEst, var.equal = TRUE))
t.test(qa.data$HalfChangeRound ~ qa.data$LeadEst, var.equal = TRUE)

mean(qa.data$DecisionScore[qa.data$LeadEst == 0])
sd(qa.data$DecisionScore[qa.data$LeadEst == 0])
mean(qa.data$DecisionScore[qa.data$LeadEst == 1])
sd(qa.data$DecisionScore[qa.data$LeadEst == 1])




