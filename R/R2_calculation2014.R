library(dplyr)

Movelevel <- read_csv("Processed_Data_2014/Movelevel.csv")

# Create a function to calculate R-squared
calculate_r_squared <- function(x, y) {
  model <- lm(y ~ x)
  return(summary(model)$r.squared)
}

# Define the columns for different categories
lf_columns <- paste0("LF", 1:10)
lq_columns <- paste0("LQ", 1:10)
lh_columns <- paste0("LH", 1:10)
lc_columns <- paste0("LC", 1:10)

# Initialize an empty data frame to store results
results_df <- data.frame(group = numeric(),
                         R2_LF = numeric(),
                         R2_LQ = numeric(),
                         R2_LH = numeric(),
                         R2_LC = numeric())

# Loop through each group
for (group_num in 1:45) {
  group_data <- Movelevel[Movelevel$group == group_num, ]
  vector1 <- c(1:nrow(group_data))  # Create the vector1 inside the loop
  
  for (i in 1:10) {
    r2_lf <- calculate_r_squared(vector1, group_data[[lf_columns[i]]])
    r2_lq <- calculate_r_squared(vector1, group_data[[lq_columns[i]]])
    r2_lh <- calculate_r_squared(vector1, group_data[[lh_columns[i]]])
    r2_lc <- calculate_r_squared(vector1, group_data[[lc_columns[i]]])
    
    results_df <- bind_rows(results_df, data.frame(group = group_num,
                                                   R2_LF = r2_lf,
                                                   R2_LQ = r2_lq,
                                                   R2_LH = r2_lh,
                                                   R2_LC = r2_lc))
  }
}

# Print the results or save them as needed
print(results_df)

# Round R-squared values to 4 decimal places
results_df$R2_LF <- round(results_df$R2_LF, 5)
results_df$R2_LQ <- round(results_df$R2_LQ, 5)
results_df$R2_LH <- round(results_df$R2_LH, 5)
results_df$R2_LC <- round(results_df$R2_LC, 5)

# Export results_df to Excel
write_xlsx(results_df, path = "R2_Results.xlsx")

summary(results_df)

############################
library(ggplot2)

# Define the variable names
variables <- c("LF", "LQ", "LH", "LC")

# Function to determine bar color based on R-squared value
get_bar_color <- function(r2_value) {
  if (r2_value < 0.3) {
    return("red")
  } else if (r2_value >= 0.3 && r2_value <= 0.7) {
    return("orange")
  } else {
    return("green")
  }
}

# Create folders for each variable
for (variable in variables) {
  dir.create(variable, showWarnings = FALSE)
  
  # Loop through round numbers
  for (round_number in 1:45) {
    # Filter the data for the specified round and variable
    plot_data <- subset(results_df, group == round_number)
    
    # Create a bar plot for the specified variable in the specified round
    plot <- ggplot(plot_data, aes(x = factor(1:10), y = get(paste0("R2_", variable)))) +
      geom_bar(stat = "identity", aes(fill = factor(ifelse(R2_LF < 0.3, "smaller 0.3",
                                                           ifelse(R2_LF <= 0.7, "between 0.3 and 0.7", "greater 0.7")))),
               position = "dodge") +
      geom_hline(yintercept = 0.3, linetype = "dashed", color = "blue") +
      geom_hline(yintercept = 0.7, linetype = "dashed", color = "blue") +
      labs(title = paste("R-squared Values for", variable, "in Round", round_number),
           x = "Player", y = "R-squared Value") +  # Update x-axis label
      scale_fill_manual(values = c("smaller 0.3" = "red", "between 0.3 and 0.7" = "orange", "greater 0.7" = "green"),
                        name = "Coefficient of determination") +  # Set legend title
      theme_minimal() +
      theme(axis.text.x = element_text(angle = 45, hjust = 1))
    
    # Save the plot as a JPEG image file
    plot_name <- sprintf("%s/%s_%02d.jpg", variable, variable, round_number)
    ggsave(plot_name, plot, width = 8, height = 6, dpi = 300)
  }
}
