##LF Scores Script
##Felicia Spaeth

library(readr)
library(dplyr)
library(matlib)

setwd("../honeycomb23/Datensicherung_Haupterhebung/Cohesion")

#### Data Cleaning ####

exceldata <- read.csv("HC_2022-03-24_17-09-10_MOVES.csv", sep = ";", header = FALSE)

#Clean columns and rows

data <- data.frame(exceldata)

data_0 <- data %>% filter(V7 != " not_moved")

data_a <- data_0 %>% select(c(1,3:5))

data_b <- data_a %>% slice(-1)

#Rename, filter and change to numeric

data_c <- data_b %>% rename("Game" = "V1",
                            "Player" = "V3",
                            "X" = "V4",
                            "Y" = "V5" )

data_d <- data_c %>% filter(X != "")

data_d$Game <- as.numeric (data_d$Game)
data_d$Player <- as.numeric (data_d$Player)
data_d$X <- as.numeric (data_d$X)
data_d$Y <- as.numeric (data_d$Y)

#Add Y2 and Z

data_e <- data_d %>% mutate(Y2 = Y * (-1), 
                            Z = (-1) * X + Y)

#Remove practice rounds

data_f <- data_e %>% filter(Game != "2")

#Identify rounds (check later again!)
Rounds <- which(data_f$Player == -1)
data_g <- data_f %>% mutate(Round = "0",.after = Game)

for (i in 1:29) {
  data_g$Round[Rounds[i]:(Rounds[i+1]-1)] <- i
}

data_g$Round[Rounds[30]:length(data_g$Round)] <- 30

#Remove round markers and reward fields

data_h <- data_g %>% filter(Player > -1 & Player < 6)

#Add LF scores 

data_i <- data_h %>% mutate(LF1 = 0, LF2 = 0, LF3 = 0, LF4 = 0, LF5 = 0, LF6 = 0,
                            LQ1 = 0, LQ2 = 0, LQ3 = 0, LQ4 = 0, LQ5 = 0, LQ6 = 0,
                            LH1 = 0, LH2 = 0, LH3 = 0, LH4 = 0, LH5 = 0, LH6 = 0)

data_j <- data_i

#### Calculate Functions ####

#Calculate Distance 

CalculateDistance <- function (coord1, coord2) {
  #difference between two coordinates
  c.diff<-coord1-coord2
  
  if(c.diff[1]<0){c.diff<-c.diff*(-1)}
  if(c.diff[2]<0){return(sum(abs(c.diff)))
  }else{return(max(abs(c.diff)))}
}

#Calculate Angle 

CalculateAngle <- function (Old, New, Other) {
  if (all(New == Other)) {
    
    Angle <- 0
    
    return(Angle)
  } 
  else {
    
    #Define vectors
    TailVector <- New - Old
    PlayerVector <- Other - New
    
    #Calculate angle
    Angle <- angle(TailVector, PlayerVector)
    
    Angle <- round(Angle)
    
    return(Angle)
  }
}

#Calculate LF 

CalculateLF <- function (Old, New, ID) {
  
  #Create matrix to save distance changes
  Change <- ifelse(Old==New, 0, New-Old)
  diag(Change) <- 0
  
  #Sum of row is sum of distance change for each player
  Sumchange <- rowSums(Change)
  
  LFvector <- Sumchange
  
  LFvector[-ID] <- -Sumchange[-ID]
  
  return(LFvector)
  
}

#Calculate LQ 

CalculateLQ <- function (Old, New, ID) {
  
  #Create matrix to save weight changes
  Change <- ifelse(Old==New, 0, New-Old)
  diag(Change) <- 0
  
  #Sum of row is sum of distance change for each player
  Sumchange <- rowSums(Change)
  
  LQvector <- Sumchange
  
  LQvector[-ID] <- -Sumchange[-ID]
  
  return(LQvector)
  
}

#Calculate LH 

CalculateLH <- function (Old, New, ID) {
  
  #Create matrix to save angles that changed, assign -1 to every angle that didn't change
  Change <- ifelse(Old==New, -1, New)
  diag(Change) <- -1
  
  #Create matrix to assign points according to the angle
  
  Points <- ifelse(Change < 0, 0, 
            ifelse(Change <= 60, -1,
            ifelse(Change <= 120, 0, 1)))
  
  #Sum of row is sum of distance change for each player
  LHvector <- rowSums(Points)
  
  return(LHvector)
  
}

#### Loop Preparation ####

#Save all player names in Player_ID

Player_ID <- sort(unique(data_i$Player))

#Create list to save history

Coord_game <- list()

#### Round Loop ####

for (i in 1:30) {
  
  # save in lines relevant rows for round i
  lines <- which(data_i$Round == i) 
  
  #Create Coordinate matrices
  Coordinate_matrix <- matrix(0,length(Player_ID), 4)
  
  colnames(Coordinate_matrix) <- c("X", "Y", "Y2", "Z")
  rownames(Coordinate_matrix) <- Player_ID
  
  Last_coordinate_matrix <- Coordinate_matrix
  
  #Create Distance matrix
  Distance_matrix <- matrix(0,length(Player_ID), length(Player_ID))
  
  colnames(Distance_matrix) <- Player_ID
  rownames(Distance_matrix) <- Player_ID
  
  #Create Weight matrix
  Weight_matrix <- matrix(0,length(Player_ID), length(Player_ID))
  
  colnames(Weight_matrix) <- Player_ID
  rownames(Weight_matrix) <- Player_ID
  
  #Create Angle matrix
  Angle_matrix <- matrix(0,length(Player_ID), length(Player_ID))
  
  colnames(Angle_matrix) <- Player_ID
  rownames(Angle_matrix) <- Player_ID
  
  #Create list to store matrices
  
  Coord_round <- list()
    
  Coord_round[[(length(Coord_round) + 1)]] <- list(Coordinate_matrix,Distance_matrix,Weight_matrix,Angle_matrix)
  
  #Create Scores for LF, LQ and LH
  
  LF <- rep(0,6)
  LQ <- rep(0,6)
  LH <- rep(0,6)

  #### Loop Move ####
  
  # for every move in round j
  for (j in lines) {
    
    #Save move in Coord
    Coord <- as.matrix(data_i[j, 3:7])
    
    #Identify moved player
    ID <- Coord[,1] + 1
    
    #Save moved coordinates
    Last_coordinate_matrix [ID,] <- Coordinate_matrix [ID,]
    Coordinate_matrix[ID,] <- Coord [1,2:5]
    
    #Calculate Weight and Angle
    
    for (k in 1:length(Player_ID)) {
      for (l in 1:length(Player_ID)) {
        
        # Fill Distance_matrix: 
        D <- CalculateDistance(Coordinate_matrix[k,1:2], Coordinate_matrix[l,1:2])
        Distance_matrix[k,l] <- D
        
        # Fill Weight_matrix: add one to avoid division by zero
        distance <- CalculateDistance(Coordinate_matrix[k,1:2], Coordinate_matrix[l,1:2]) + 1
        W <- 1/distance
        Weight_matrix[k,l] <- W
        
        #Fill Angle_matrix
        
        if (!all(Last_coordinate_matrix[k,] == Coordinate_matrix[k,])) {
          
          A <- CalculateAngle(Last_coordinate_matrix[k,c(1,3,4)], Coordinate_matrix[k,c(1,3,4)], Coordinate_matrix[l,c(1,3,4)])
          Angle_matrix[k,l] <- A
        }
      }
    }
    
    # Fill LF into table
    LFchange <- CalculateLF(Coord_round[[length(Coord_round)]][[2]], Distance_matrix, ID)
    
    LF <- LF + LFchange

    a <- c("LF1", "LF2", "LF3", "LF4", "LF5", "LF6") #necessary?
    
    if (all(colnames(data_j[,8:13]) == a)){
      
      data_j[j, 8:13] <- LF
      
    }
    
    # Fill LQ into table
    LQchange <- CalculateLQ(Coord_round[[length(Coord_round)]][[3]], Weight_matrix, ID)
    
    LQ <- LQ + LQchange
    
    a <- c("LQ1", "LQ2", "LQ3", "LQ4", "LQ5", "LQ6") #necessary?
    
    if (all(colnames(data_j[,14:19]) == a)){
      
      data_j[j, 14:19] <- LQ
      
    }
    
    # Fill LH into table
    LHchange <- CalculateLH(Coord_round[[length(Coord_round)]][[4]], Angle_matrix, ID)
    
    LH <- LH + LHchange
    
    a <- c("LH1", "LH2", "LH3", "LH4", "LH5", "LH6") #necessary?
    
    if (all(colnames(data_j[,20:25]) == a)){
      
      data_j[j, 20:25] <- LH
      
    }
    
    #Save Coordinates and Weights in list
    Coord_round[[length(Coord_round) + 1]] <- list(Coordinate_matrix,Distance_matrix,Weight_matrix,Angle_matrix) 
  }
  Coord_game[[i]] <- Coord_round
}

#### Round LQ ####

data_k <- data_j
  
data_k[,14:19] <- lapply(data_k[,14:19], round, 2)


# Script for one excel file 