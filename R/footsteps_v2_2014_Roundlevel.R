library(tidyverse)

#functions at the end of document


df_in <- read_csv("~/honeycomb23/Processed_Data_2014/Movelevel.csv")

summary(df_in)

df_in <- df_in [c("group","player","sj1", "sj2")]
df_in <- df_in %>% rename(round = group)


#df_in <- df_in[1:79,]

df_in$sj1 <- df_in$sj1 +6
df_in$sj2 <- df_in$sj2 +6
df_in$player <- df_in$player + 1

str(df_in)
resultIN <- function_scoring(df_in)
colSums(resultIN[,1:10])


resultIN


subset_df <- resultIN[, 1:10]

FSScores <- as.vector(t(subset_df))

untereinander <- data.frame(new_column_name = FSScores)



resultIN[1,2]

#################

# Assuming df is your main dataframe and it has a column named 'round' to identify each round

# Initialize an empty matrix to store scores
score_matrix <- matrix(0, nrow = 45, ncol = 10)
colnames(score_matrix) <- paste0("Player_", 1:10)
rownames(score_matrix) <- paste0("Round_", 1:45)

score_matrix[1, ] <- resultIN
resultIN

# Loop through each round
for (r in 1:45) {
  # Subset data for the current round
  round_data <- df_in[df_in$round == r, ]
  
  # Calculate scores for the current round using your scoring function
  round_scores <- function_scoring(round_data)
  
  # Store the scores in the matrix
  score_matrix[r, ] <- round_scores
}

# Convert the matrix to a dataframe for easier manipulation and viewing
score_df <- as.data.frame(score_matrix)



#work around
df_in <- df_in[110:1472,]

df_in
resultIN <- function_scoring(df_in)
colSums(resultIN[,1:6])















#only per round
function_scoring <- function(data) {
  
  # initialize variables
  rounds <- unique(data$round)
  players <- 1:10
  scores <- matrix(0, nrow = length(rounds), ncol = length(players))
  colnames(scores) <- paste("player", players)
  
  for (i in rounds) {
    round_data <- data[data$round == i & !is.na(data$sj1), ]
    field_matrix <- matrix(0, nrow = 13, ncol = 13)
    moves <- split(round_data, round_data$player)
    
    for (j in players) {
      player_moves <- moves[[j]]
      x <- player_moves$sj1
      y <- player_moves$sj2
      for (k in 2:nrow(player_moves)) {
        if (x[k] != x[k - 1] || y[k] != y[k - 1]) {
          field_matrix[x[k] + 1, y[k] + 1] <- field_matrix[x[k] + 1, y[k] + 1] + 1
          scores[i - min(rounds) + 1, j] <- scores[i - min(rounds) + 1, j] - field_matrix[x[k] + 1, y[k] + 1]
        }
      }
      scores[i - min(rounds) + 1, j] <- scores[i - min(rounds) + 1, j] + sum(field_matrix[x + 1, y + 1] == 1)
    }
  }
  
  scores_df <- as.data.frame(scores)
  scores_df$round <- rounds
  
  return(scores_df)
}

function_scoring(df)

function_scoring_move <- function(data) {
  
  # initialize variables
  rounds <- unique(data$round)
  players <- 1:6
  scores_round <- matrix(0, nrow = length(rounds), ncol = length(players))
  scores_move <- matrix(0, nrow = sum(!is.na(data$sj1)), ncol = length(players))
  colnames(scores_round) <- paste("player", players)
  colnames(scores_move) <- paste("player", players)
  
  kk <- 0
  for (i in rounds) {
    round_data <- data[data$round == i & !is.na(data$sj1), ]
    field_matrix <- matrix(0, nrow = 11, ncol = 11)
    moves <- split(round_data, round_data$player)
    
    for (j in players) {
      player_moves <- moves[[j]]
      x <- player_moves$sj1
      y <- player_moves$sj2
      for (k in 2:nrow(player_moves)) {
        kk <- kk + 1
        if (x[k] != x[k - 1] || y[k] != y[k - 1]) {
          field_matrix[x[k] + 1, y[k] + 1] <- field_matrix[x[k] + 1, y[k] + 1] + 1
          scores_round[i - min(rounds) + 1, j] <- scores_round[i - min(rounds) + 1, j] - field_matrix[x[k] + 1, y[k] + 1]
          scores_move[kk, j] <- scores_move[kk, j] - field_matrix[x[k] + 1, y[k] + 1]
        }
      }
      scores_round[i - min(rounds) + 1, j] <- scores_round[i - min(rounds) + 1, j] + sum(field_matrix[x + 1, y + 1] == 1)
      scores_move[kk, j] <- scores_move[kk, j] + sum(field_matrix[x + 1, y + 1] == 1)
    }
  }
  
  scores_round_df <- as.data.frame(scores_round)
  scores_round_df$round <- rounds
  scores_move_df <- as.data.frame(scores_move)
  scores_move_df$move <- 1:nrow(scores_move_df)
  
  return(list(round = scores_round_df, move = scores_move_df))
}



###per move


function_scoring_per_move <- function(data) {
  
  # initialize variables
  rounds <- unique(data$round)
  players <- 1:6
  scores_round <- matrix(0, nrow = length(rounds), ncol = length(players))
  colnames(scores_round) <- paste("player", players)
  
  scores_move <- data.frame(round = numeric(0), move = numeric(0), score = numeric(0), player = numeric(0))
  
  move_idx <- 1
  
  for (i in rounds) {
    round_data <- data[data$round == i & !is.na(data$sj1), ]
    field_matrix <- matrix(0, nrow = 11, ncol = 11)
    moves <- split(round_data, round_data$player)
    
    for (j in players) {
      player_moves <- moves[[j]]
      x <- player_moves$sj1
      y <- player_moves$sj2
      for (k in 2:nrow(player_moves)) {
        if (x[k] != x[k - 1] || y[k] != y[k - 1]) {
          field_matrix[x[k] + 1, y[k] + 1] <- field_matrix[x[k] + 1, y[k] + 1] + 1
          scores_round[i - min(rounds) + 1, j] <- scores_round[i - min(rounds) + 1, j] - field_matrix[x[k] + 1, y[k] + 1]
        }
        
        scores_move[move_idx, ] <- c(i, k, scores_round[i - min(rounds) + 1, j], j)
        move_idx <- move_idx + 1
      }
      scores_round[i - min(rounds) + 1, j] <- scores_round[i - min(rounds) + 1, j] + sum(field_matrix[x + 1, y + 1] == 1)
    }
  }
  
  scores_round_df <- as.data.frame(scores_round)
  scores_round_df$round <- rounds
  
  return(list(scores_round = scores_round_df, scores_move = scores_move))
}


ergebnis <- function_scoring_per_move(df)
